<?php
require_once "db.inc.php";
echo '<?xml version="1.0" encoding="UTF-8" ?>';

// Ensure the userid post item exists
if(!isset($_POST['user'])) {
    echo '<hatter status="no" msg="missing user" />';
    exit;
}
// Ensure the password post item exists
if(!isset($_POST['pw'])) {
    echo '<hatter status="no" msg="missing password" />';
    exit;
}
$user = $_POST["user"];
$password = $_POST["pw"];
$pdo = pdo_connect();
$userid = getUser($pdo, $user, $password);

function getUser($pdo, $user, $password) {
    // Does the user exist in the database?
    $userQ = $pdo->quote($user);
    $query = "SELECT id, password from connect where user=$userQ";
    $rows = $pdo->query($query);
    if($row = $rows->fetch()) {
        // We found the record in the database
        // Check the password
        if($row['password'] != $password) {
            echo '<hatter status="no" msg="password error" />';
            exit;
        }

        return $row['id'];
    }

    echo '<hatter status="no" msg="user error" />';
    exit;
}

function processXml($pdo, $userid, $xmltext) {

}
